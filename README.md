# php-extended/php-version-object
An implementation of the php-version-interface library

![coverage](https://gitlab.com/php-extended/php-version-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-version-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-version-object ^8`


## Basic Usage

You may build version numbers objects with the following code :

```php

use PhpExtended\Version\Version;

$version = new Version(1, 0, 0, "alpha");   // 1.0.0-alpha
$version = $version->incrementPatch();      // 1.0.1
$version = $version->incrementMinor();      // 1.1.0
$version = $version->incrementMajor();      // 2.0.0

```

or with constraints and ranges :

```php

use PhpExtended\PhpVersion\VersionConstraintSimple;
use PhpExtended\PhpVersion\VersionOperatorHigherEquals;

$constraint = new VersionConstraintSimple(
	new VersionOperatorHigherEquals(),
	new Version(2, 3, 4)
); // >=2.3.4

$range = $constraint->getRange(); // [2.3.4, +∞[

$range->containsVersion(new Version(3, 0, 0)); // true
```


## License

MIT (See [license file](LICENSE)).
