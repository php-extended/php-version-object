<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Version\Version;
use PhpExtended\Version\VersionBoundary;
use PhpExtended\Version\VersionConstraintSimple;
use PhpExtended\Version\VersionConstraintUnion;
use PhpExtended\Version\VersionOperatorNextMajor;
use PhpExtended\Version\VersionRange;
use PhpExtended\Version\VersionSegment;
use PHPUnit\Framework\TestCase;

/**
 * VersionConstraintUnionTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Version\VersionConstraintUnion
 *
 * @internal
 *
 * @small
 */
class VersionConstraintUnionTest extends TestCase
{
	
	/**
	 * The constraint to test.
	 * 
	 * @var VersionConstraintUnion
	 */
	protected VersionConstraintUnion $_constraint;
	
	public function testToString() : void
	{
		$this->assertEquals('( ^3.2.1 || ^4.0.0 )', $this->_constraint->__toString());
	}
	
	public function testOperator() : void
	{
		$this->assertEquals(new VersionOperatorNextMajor(), $this->_constraint->getOperator());
	}
	
	public function testRange() : void
	{
		$expected = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(3, 2, 1), true), new VersionBoundary(new Version(5, 0, 0), false)),
		]);
		$this->assertEquals($expected, $this->_constraint->getRange());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_constraint->equals($this->_constraint));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_constraint = new VersionConstraintUnion(
			new VersionConstraintSimple(new VersionOperatorNextMajor(), new Version(3, 2, 1)),
			new VersionConstraintSimple(new VersionOperatorNextMajor(), new Version(4, 0, 0)),
		);
	}
	
}
