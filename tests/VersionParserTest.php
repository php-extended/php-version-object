<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Version\Version;
use PhpExtended\Version\VersionParser;
use PHPUnit\Framework\TestCase;

/**
 * VersionParserTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Version\VersionParser
 *
 * @internal
 *
 * @small
 */
class VersionParserTest extends TestCase
{
	
	/**
	 * The parser to test.
	 * 
	 * @var VersionParser
	 */
	protected VersionParser $_parser;
	
	public function testParseNull() : void
	{
		$this->assertEquals(new Version(0, 0, 0), $this->_parser->parse(null));
	}
	
	public function testParseMajor() : void
	{
		$this->assertEquals(new Version(2, 0, 0), $this->_parser->parse('2'));
	}
	
	public function testParseMinor() : void
	{
		$this->assertEquals(new Version(2, 3, 0), $this->_parser->parse('2.3'));
	}
	
	public function testParsePatch() : void
	{
		$this->assertEquals(new Version(2, 3, 4), $this->_parser->parse('2.3.4'));
	}
	
	public function testParseLabel() : void
	{
		$this->assertEquals(new Version(2, 3, 4, 'alpha'), $this->_parser->parse('2.3.4-alpha'));
	}
	
	public function testParseLabel2() : void
	{
		$this->assertEquals(new Version(2, 3, 4, '42'), $this->_parser->parse('2.3.4.42'));
	}
	
	public function testParseVersionPrefix() : void
	{
		$this->assertEquals(new Version(2, 3, 4), $this->_parser->parse('v2.3.4'));
	}
	
	public function testParseVersionBuild() : void
	{
		$this->assertEquals(new Version(5, 0, 1, '20200921'), $this->_parser->parse('5.0.1+20200921'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_parser = new VersionParser();
	}
	
}
