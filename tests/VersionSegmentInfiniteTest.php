<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Version\Version;
use PhpExtended\Version\VersionBoundary;
use PhpExtended\Version\VersionRange;
use PhpExtended\Version\VersionSegment;
use PHPUnit\Framework\TestCase;

/**
 * VersionSegmentInfiniteTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Version\VersionSegment
 *
 * @internal
 *
 * @small
 */
class VersionSegmentInfiniteTest extends TestCase
{
	
	/**
	 * The range to test.
	 *
	 * @var VersionSegment
	 */
	protected VersionSegment $_segment;
	
	public function testToString() : void
	{
		$this->assertEquals('[2.3.4, +∞[', $this->_segment->__toString());
	}
	
	public function testLowerBound() : void
	{
		$this->assertEquals(new VersionBoundary(new Version(2, 3, 4), true), $this->_segment->getLowerBound());
	}
	
	public function testUpperBound() : void
	{
		$this->assertNull($this->_segment->getUpperBound());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_segment->equals($this->_segment));
	}
	
	public function testContainsVersion() : void
	{
		$this->assertTrue($this->_segment->containsVersion($this->_segment->getLowerBound()->getBaseVersion()));
	}
	
	public function testDoesNotContainsVersion() : void
	{
		$this->assertFalse($this->_segment->containsVersion(new Version(0, 0, 0)));
	}
	
	public function testContainsBoundary() : void
	{
		$this->assertTrue($this->_segment->containsBoundary(new VersionBoundary(new Version(3, 4, 5), false)));
	}
	
	public function testDoesNotContainsBoundaryLower() : void
	{
		$this->assertFalse($this->_segment->containsBoundary(new VersionBoundary(new Version(1, 0, 0), false)));
	}
	
	public function testContainsSegment() : void
	{
		$this->assertTrue($this->_segment->containsSegment($this->_segment));
	}
	
	public function testDoesContainsInfiniteBoundary() : void
	{
		$this->assertTrue($this->_segment->containsBoundary(null));
	}
	
	public function testUnion() : void
	{
		$this->assertEquals(new VersionRange([$this->_segment]), $this->_segment->union($this->_segment));
	}
	
	public function testUnion2() : void
	{
		$rangeAll = new VersionSegment(new VersionBoundary(new Version(0, 0, 0), true), null);
		$this->assertEquals(new VersionRange([$rangeAll]), $this->_segment->union($rangeAll));
	}
	
	public function testUnion3() : void
	{
		$other = new VersionSegment(new VersionBoundary(new Version(0, 0, 0), true), new VersionBoundary(new Version(1, 0, 0), false));
		$this->assertEquals(new VersionRange([$other, $this->_segment]), $this->_segment->union($other));
	}
	
	public function testIntersection() : void
	{
		$this->assertEquals(new VersionRange([$this->_segment]), $this->_segment->intersect($this->_segment));
	}
	
	public function testIntersection2() : void
	{
		$rangeAll = new VersionSegment(new VersionBoundary(new Version(0, 0, 0), true), null);
		$this->assertEquals(new VersionRange([$this->_segment]), $this->_segment->intersect($rangeAll));
	}
	
	public function testIntersection3() : void
	{
		$other = new VersionSegment(new VersionBoundary(new Version(5, 0, 0), true), new VersionBoundary(new Version(6, 0, 0), false));
		$expected = new VersionRange([$other]);
		$this->assertEquals($expected, $this->_segment->intersect($other));
	}
	
	public function testIntersection4() : void
	{
		$other = new VersionSegment(new VersionBoundary(new Version(1, 2, 3), true), new VersionBoundary(new Version(3, 0, 0), false));
		$expected = new VersionSegment(new VersionBoundary(new Version(2, 3, 4), true), new VersionBoundary(new Version(3, 0, 0), false));
		$this->assertEquals(new VersionRange([$expected]), $this->_segment->intersect($other));
	}
	
	public function testSubtract1() : void
	{
		$this->assertEquals(new VersionRange([]), $this->_segment->subtract($this->_segment));
	}
	
	public function testSubtract2() : void
	{
		$interval = new VersionSegment(new VersionBoundary(new Version(5, 0, 0), true), null);
		$expected = new VersionSegment(new VersionBoundary(new Version(2, 3, 4), true), new VersionBoundary(new Version(5, 0, 0), false));
		$this->assertEquals(new VersionRange([$expected]), $this->_segment->subtract($interval));
	}
	
	public function testSubtract21() : void
	{
		$interval = new VersionSegment(new VersionBoundary(new Version(0, 0, 0), true), new VersionBoundary(new Version(1, 0, 0), false));
		$this->assertEquals(new VersionRange([$this->_segment]), $this->_segment->subtract($interval));
	}
	
	public function testSubtract3() : void
	{
		$interval = new VersionSegment(new VersionBoundary(new Version(1, 0, 0), true), new VersionBoundary(new Version(3, 0, 0), false));
		$expected = new VersionSegment(new VersionBoundary(new Version(3, 0, 0), true), null);
		$this->assertEquals(new VersionRange([$expected]), $this->_segment->subtract($interval));
	}
	
	public function testSubtract4() : void
	{
		$interval = new VersionSegment(new VersionBoundary(new Version(5, 0, 0), true), new VersionBoundary(new Version(6, 0, 0), false));
		$expected = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(2, 3, 4), true), new VersionBoundary(new Version(5, 0, 0), false)),
			new VersionSegment(new VersionBoundary(new Version(6, 0, 0), true), null),
		]);
		$this->assertEquals($expected, $this->_segment->subtract($interval));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_segment = new VersionSegment(
			new VersionBoundary(new Version(2, 3, 4), true),
			null,
		);
	}
	
}
