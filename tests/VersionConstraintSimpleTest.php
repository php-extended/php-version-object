<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Version\Version;
use PhpExtended\Version\VersionBoundary;
use PhpExtended\Version\VersionConstraintSimple;
use PhpExtended\Version\VersionOperatorHigherEquals;
use PhpExtended\Version\VersionRange;
use PhpExtended\Version\VersionSegment;
use PHPUnit\Framework\TestCase;

/**
 * VersionConstraintSimpleTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Version\VersionConstraintSimple
 *
 * @internal
 *
 * @small
 */
class VersionConstraintSimpleTest extends TestCase
{
	
	/**
	 * The constraint to test.
	 * 
	 * @var VersionConstraintSimple
	 */
	protected VersionConstraintSimple $_constraint;
	
	public function testToString() : void
	{
		$this->assertEquals('>=2.3.4', $this->_constraint->__toString());
	}
	
	public function testOperator() : void
	{
		$this->assertEquals(new VersionOperatorHigherEquals(), $this->_constraint->getOperator());
	}
	
	public function testRange() : void
	{
		$expected = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(2, 3, 4), true), null),
		]);
		$this->assertEquals($expected, $this->_constraint->getRange());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_constraint->equals($this->_constraint));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_constraint = new VersionConstraintSimple(new VersionOperatorHigherEquals(), new Version(2, 3, 4));
	}
	
}
