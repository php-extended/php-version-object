<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Version\Version;
use PHPUnit\Framework\TestCase;

/**
 * VersionTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Version\Version
 *
 * @internal
 *
 * @small
 */
class VersionTest extends TestCase
{
	
	/**
	 * The version to test.
	 * 
	 * @var Version
	 */
	protected Version $_version;
	
	public function testToString() : void
	{
		$this->assertEquals('2.3.4-beta-2', $this->_version->__toString());
	}
	
	public function testMajor() : void
	{
		$this->assertEquals(2, $this->_version->getMajor());
	}
	
	public function testMinor() : void
	{
		$this->assertEquals(3, $this->_version->getMinor());
	}
	
	public function testPatch() : void
	{
		$this->assertEquals(4, $this->_version->getPatch());
	}
	
	public function testLabel() : void
	{
		$this->assertEquals('beta-2', $this->_version->getLabel());
	}
	
	public function testToInteger() : void
	{
		$this->assertEquals(20304, $this->_version->toInteger());
	}
	
	public function testToArray() : void
	{
		$this->assertEquals(['2', '3', '4', 'beta-2'], $this->_version->toArray());
	}
	
	public function testIsDev() : void
	{
		$this->assertFalse($this->_version->isDeveloppement());
	}
	
	public function testIncrementMajor() : void
	{
		$this->assertEquals(new Version(3, 0, 0), $this->_version->incrementMajor());
	}
	
	public function testIncrementMinor() : void
	{
		$this->assertEquals(new Version(2, 4, 0), $this->_version->incrementMinor());
	}
	
	public function testIncrementPatch() : void
	{
		$this->assertEquals(new Version(2, 3, 5), $this->_version->incrementPatch());
	}
	
	public function testIncrementLabel() : void
	{
		$this->assertEquals(new Version(2, 3, 4, 'rc'), $this->_version->setLabel('rc'));
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_version->equals(new Version(2, 3, 4, 'beta-2')));
	}
	
	public function testMin() : void
	{
		$this->assertEquals($this->_version, $this->_version->min($this->_version->incrementMajor()));
	}
	
	public function testMin2() : void
	{
		$other = new Version(2, 3, 4);
		$this->assertEquals($other, $this->_version->min($other));
	}
	
	public function testMax() : void
	{
		$other = $this->_version->incrementMajor();
		$this->assertEquals($other, $this->_version->max($other));
	}
	
	public function testMax2() : void
	{
		$other = new Version(2, 3, 4);
		$this->assertEquals($this->_version, $this->_version->max($other));
	}
	
	public function testIsStrictlyGreaterMajor() : void
	{
		$this->assertTrue($this->_version->incrementMajor()->isStrictlyGreaterThan($this->_version));
	}
	
	public function testIsStrictlyGreaterMajor2() : void
	{
		$this->assertFalse($this->_version->isStrictlyGreaterThan($this->_version->incrementMajor()));
	}
	
	public function testIsStrictlyGreaterMinor() : void
	{
		$this->assertTrue($this->_version->incrementMinor()->isStrictlyGreaterThan($this->_version));
	}
	
	public function testIsStrictlyGreaterMinor2() : void
	{
		$this->assertFalse($this->_version->isStrictlyGreaterThan($this->_version->incrementMinor()));
	}
	
	public function testIsStrictlyGreaterPatch() : void
	{
		$this->assertTrue($this->_version->incrementPatch()->isStrictlyGreaterThan($this->_version));
	}
	
	public function testIsStrictlyGreaterPatch2() : void
	{
		$this->assertFalse($this->_version->isStrictlyGreaterThan($this->_version->incrementPatch()));
	}
	
	public function testIsStrictlyGreaterLabel() : void
	{
		$this->assertTrue((new Version(2, 3, 4, 'rc'))->isStrictlyGreaterThan($this->_version));
	}
	
	public function testIsStrictlyGreaterLabel2() : void
	{
		$this->assertTrue((new Version(2, 3, 4, 'beta-4'))->isStrictlyGreaterThan($this->_version));
	}
	
	public function testIsStrictlyGreaterLabel3() : void
	{
		$this->assertFalse((new Version(2, 3, 4, 'beta'))->isStrictlyGreaterThan($this->_version));
	}
	
	public function testIsStrictlyGreaterLabel4() : void
	{
		$this->assertTrue((new Version(2, 3, 4, 'bbb'))->isStrictlyGreaterThan(new Version(2, 3, 4, 'aaa')));
	}
	
	public function testGreaterEquals() : void
	{
		$this->assertTrue($this->_version->isGreaterThanOrEquals($this->_version));
	}
	
	public function testLowerEquals() : void
	{
		$this->assertTrue($this->_version->isLowerThanOrEquals($this->_version));
	}
	
	public function testStrictlyLowerEquals() : void
	{
		$this->assertFalse($this->_version->isStrictlyLowerThan($this->_version));
	}
	
	public function testIsStrictlyGreaterEquals() : void
	{
		$this->assertFalse($this->_version->isStrictlyGreaterThan($this->_version));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_version = new Version(2, 3, 4, 'beta-2');
	}
	
}
