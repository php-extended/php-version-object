<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Version\Version;
use PhpExtended\Version\VersionConstraintIntersection;
use PhpExtended\Version\VersionConstraintParser;
use PhpExtended\Version\VersionConstraintSimple;
use PhpExtended\Version\VersionConstraintUnion;
use PhpExtended\Version\VersionOperatorDifferent;
use PhpExtended\Version\VersionOperatorEquals;
use PhpExtended\Version\VersionOperatorHigherEquals;
use PhpExtended\Version\VersionOperatorLowerEquals;
use PhpExtended\Version\VersionOperatorNextMajor;
use PhpExtended\Version\VersionOperatorNextMinor;
use PhpExtended\Version\VersionOperatorNextPatch;
use PhpExtended\Version\VersionOperatorStrictlyHigher;
use PhpExtended\Version\VersionOperatorStrictlyLower;
use PHPUnit\Framework\TestCase;

/**
 * VersionConstraintParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Version\VersionConstraintParser
 *
 * @internal
 *
 * @small
 */
class VersionConstraintParserTest extends TestCase
{
	
	/**
	 * The parser to test.
	 * 
	 * @var VersionConstraintParser
	 */
	protected VersionConstraintParser $_parser;
	
	public function testRangeEmpty() : void
	{
		$this->assertEquals(new VersionConstraintSimple(new VersionOperatorEquals(), new Version(0, 0, 0)), $this->_parser->parse(null));
	}
	
	public function testRangeSpace() : void
	{
		$this->assertEquals(new VersionConstraintSimple(new VersionOperatorEquals(), new Version(2, 0, 0)), $this->_parser->parse('   2   '));
	}
	
	public function testRangeParenth() : void
	{
		$this->assertEquals(new VersionConstraintSimple(new VersionOperatorEquals(), new Version(0, 0, 0)), $this->_parser->parse('()'));
	}
	
	public function testRangeImplicitEquals() : void
	{
		$this->assertEquals(new VersionConstraintSimple(new VersionOperatorEquals(), new Version(2, 3, 4)), $this->_parser->parse('2.3.4'));
	}
	
	public function testRangeExplicitEquals() : void
	{
		$this->assertEquals(new VersionConstraintSimple(new VersionOperatorEquals(), new Version(2, 3, 4)), $this->_parser->parse('=2.3.4'));
	}
	
	public function testRangeStrictlyGreater() : void
	{
		$this->assertEquals(new VersionConstraintSimple(new VersionOperatorStrictlyHigher(), new Version(2, 3, 4)), $this->_parser->parse('>2.3.4'));
	}
	
	public function testRangeGreaterEquals() : void
	{
		$this->assertEquals(new VersionConstraintSimple(new VersionOperatorHigherEquals(), new Version(2, 3, 4)), $this->_parser->parse('>=2.3.4'));
	}
	
	public function testRangeStrictlyLower() : void
	{
		$this->assertEquals(new VersionConstraintSimple(new VersionOperatorStrictlyLower(), new Version(2, 3, 4)), $this->_parser->parse('<2.3.4'));
	}
	
	public function testRangeLowerEquals() : void
	{
		$this->assertEquals(new VersionConstraintSimple(new VersionOperatorLowerEquals(), new Version(2, 3, 4)), $this->_parser->parse('<=2.3.4'));
	}
	
	public function testRangeDifferent() : void
	{
		$this->assertEquals(new VersionConstraintSimple(new VersionOperatorDifferent(), new Version(2, 3, 4)), $this->_parser->parse('!2.3.4'));
	}
	
	public function testRangeDifferent2() : void
	{
		$this->assertEquals(new VersionConstraintSimple(new VersionOperatorDifferent(), new Version(2, 3, 4)), $this->_parser->parse('!=2.3.4'));
	}
	
	public function testRangeDifferent3() : void
	{
		$this->assertEquals(new VersionConstraintSimple(new VersionOperatorDifferent(), new Version(2, 3, 4)), $this->_parser->parse('<>2.3.4'));
	}
	
	public function testRangeAnd() : void
	{
		$expected = new VersionConstraintIntersection(
			new VersionConstraintSimple(new VersionOperatorHigherEquals(), new Version(1, 0, 0)),
			new VersionConstraintSimple(new VersionOperatorStrictlyLower(), new Version(1, 1, 0)),
		);
		
		$this->assertEquals($expected, $this->_parser->parse('>=1.0 <1.1'));
	}
	
	public function testRangeAndEsper() : void
	{
		$expected = new VersionConstraintIntersection(
			new VersionConstraintSimple(new VersionOperatorHigherEquals(), new Version(1, 0, 0)),
			new VersionConstraintSimple(new VersionOperatorStrictlyLower(), new Version(1, 1, 0)),
		);
		
		$this->assertEquals($expected, $this->_parser->parse('>=1.0 & <1.1'));
	}
	
	public function testRangeAndAnd() : void
	{
		$expected = new VersionConstraintIntersection(
			new VersionConstraintSimple(new VersionOperatorHigherEquals(), new Version(1, 0, 0)),
			new VersionConstraintSimple(new VersionOperatorStrictlyLower(), new Version(1, 1, 0)),
		);
		
		$this->assertEquals($expected, $this->_parser->parse('>=1.0 && <1.1'));
	}
	
	public function testRangeAndComma() : void
	{
		$expected = new VersionConstraintIntersection(
			new VersionConstraintSimple(new VersionOperatorHigherEquals(), new Version(1, 0, 0)),
			new VersionConstraintSimple(new VersionOperatorStrictlyLower(), new Version(1, 1, 0)),
		);
		
		$this->assertEquals($expected, $this->_parser->parse('>=1.0, <1.1'));
	}
	
	public function testRangeOrVbar() : void
	{
		$expected = new VersionConstraintUnion(
			new VersionConstraintSimple(new VersionOperatorHigherEquals(), new Version(1, 0, 0)),
			new VersionConstraintSimple(new VersionOperatorStrictlyLower(), new Version(1, 1, 0)),
		);
		
		$this->assertEquals($expected, $this->_parser->parse('>=1.0 | <1.1'));
	}
	
	public function testRangeOrOr() : void
	{
		$expected = new VersionConstraintUnion(
			new VersionConstraintSimple(new VersionOperatorHigherEquals(), new Version(1, 0, 0)),
			new VersionConstraintSimple(new VersionOperatorStrictlyLower(), new Version(1, 1, 0)),
		);
		
		$this->assertEquals($expected, $this->_parser->parse('>=1.0 || <1.1'));
	}
	
	public function testRangeComposite() : void
	{
		$expected = new VersionConstraintUnion(
			new VersionConstraintIntersection(
				new VersionConstraintSimple(new VersionOperatorHigherEquals(), new Version(1, 0, 0)),
				new VersionConstraintSimple(new VersionOperatorStrictlyLower(), new Version(1, 1, 0)),
			),
			new VersionConstraintSimple(new VersionOperatorHigherEquals(), new Version(1, 2, 0)),
		);
		
		$this->assertEquals($expected, $this->_parser->parse('>=1.0 <1.1 || >=1.2'));
	}
	
	public function testRangeCompositeParenth() : void
	{
		$expected = new VersionConstraintUnion(
			new VersionConstraintSimple(new VersionOperatorHigherEquals(), new Version(1, 2, 0)),
			new VersionConstraintIntersection(
				new VersionConstraintSimple(new VersionOperatorHigherEquals(), new Version(1, 0, 0)),
				new VersionConstraintSimple(new VersionOperatorStrictlyLower(), new Version(1, 1, 0)),
			),
		);
		
		$this->assertEquals($expected, $this->_parser->parse('>=1.2 || ( >=1.0 <1.1 )'));
	}
	
	public function testRangeStarAll() : void
	{
		$expected = new VersionConstraintSimple(new VersionOperatorHigherEquals(), new Version(0, 0, 0));
		
		$this->assertEquals($expected, $this->_parser->parse('*'));
	}
	
	public function testRangeStarMajor() : void
	{
		$expected = new VersionConstraintSimple(new VersionOperatorNextMajor(), new Version(1, 0, 0));
		
		$this->assertEquals($expected, $this->_parser->parse('1.*'));
	}
	
	public function testRangeStarMinor() : void
	{
		$expected = new VersionConstraintSimple(new VersionOperatorNextMinor(), new Version(1, 2, 0));
		
		$this->assertEquals($expected, $this->_parser->parse('1.2.*'));
	}
	
	public function testRangeStarPatch() : void
	{
		$expected = new VersionConstraintSimple(new VersionOperatorNextPatch(), new Version(1, 2, 3));
		
		$this->assertEquals($expected, $this->_parser->parse('1.2.3.*'));
	}
	
	public function testRangeNextMajor() : void
	{
		$expected = new VersionConstraintSimple(new VersionOperatorNextMajor(), new Version(2, 0, 0));
		
		$this->assertEquals($expected, $this->_parser->parse('~2'));
	}
	
	public function testRangeNextMinor() : void
	{
		$expected = new VersionConstraintSimple(new VersionOperatorNextMinor(), new Version(2, 3, 0));
		
		$this->assertEquals($expected, $this->_parser->parse('~2.3'));
	}
	
	public function testRangeNextPatch() : void
	{
		$expected = new VersionConstraintSimple(new VersionOperatorNextPatch(), new Version(2, 3, 4));
		
		$this->assertEquals($expected, $this->_parser->parse('~2.3.4'));
	}
	
	public function testRangeNextMajorSemantic() : void
	{
		$expected = new VersionConstraintSimple(new VersionOperatorNextMajor(), new Version(2, 3, 4));
		
		$this->assertEquals($expected, $this->_parser->parse('^2.3.4'));
	}
	
	public function testRangeNextMinorSemantic() : void
	{
		$expected = new VersionConstraintSimple(new VersionOperatorNextMinor(), new Version(0, 2, 3));
		
		$this->assertEquals($expected, $this->_parser->parse('^0.2.3'));
	}
	
	public function testRangeMulti() : void
	{
		$expected = new VersionConstraintSimple(new VersionOperatorNextMajor(), new Version(2, 3, 4));
		
		$this->assertEquals($expected, $this->_parser->parse('(^2.3.4)'));
	}
	
	public function testRangeMulti2() : void
	{
		$expected = new VersionConstraintIntersection(
			new VersionConstraintSimple(new VersionOperatorNextMajor(), new Version(2, 3, 4)),
			new VersionConstraintSimple(new VersionOperatorNextMajor(), new Version(3, 4, 5)),
		);
		
		$this->assertEquals($expected, $this->_parser->parse('^2.3.4 (^3.4.5)'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_parser = new VersionConstraintParser();
	}
	
}
