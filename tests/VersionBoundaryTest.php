<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Version\Version;
use PhpExtended\Version\VersionBoundary;
use PHPUnit\Framework\TestCase;

/**
 * VersionBoundaryTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Version\VersionBoundary
 *
 * @internal
 *
 * @small
 */
class VersionBoundaryTest extends TestCase
{
	
	/**
	 * The boundary to test.
	 * 
	 * @var VersionBoundary
	 */
	protected VersionBoundary $_boundary;
	
	public function testToString() : void
	{
		$this->assertEquals('!2.3.4', $this->_boundary->__toString());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_boundary->equals(new VersionBoundary(new Version(2, 3, 4), false)));
	}
	
	public function testEquals2() : void
	{
		$this->assertFalse($this->_boundary->equals(new VersionBoundary(new Version(2, 3, 4), true)));
	}
	
	public function testVersion() : void
	{
		$this->assertEquals(new Version(2, 3, 4), $this->_boundary->getBaseVersion());
	}
	
	public function testIncluded() : void
	{
		$this->assertFalse($this->_boundary->isIncluded());
	}
	
	public function testMinMinNull() : void
	{
		$this->assertEquals($this->_boundary, $this->_boundary->minMin(null));
	}
	
	public function testMinMin1() : void
	{
		$expected = new VersionBoundary(new Version(2, 3, 5), true);
		$this->assertEquals($this->_boundary, $this->_boundary->minMin($expected));
	}
	
	public function testMinMin2() : void
	{
		$expected = new VersionBoundary(new Version(2, 3, 3), true);
		$this->assertEquals($expected, $this->_boundary->minMin($expected));
	}
	
	public function testMinMin3() : void
	{
		$expected = new VersionBoundary(new Version(2, 3, 4), true);
		$this->assertEquals($expected, $this->_boundary->minMin($expected));
	}
	
	public function testMinMin4() : void
	{
		$expected = new VersionBoundary(new Version(2, 3, 4), true);
		$this->assertEquals($expected, $expected->minMin($this->_boundary));
	}
	
	public function testMinMaxNull() : void
	{
		$this->assertEquals($this->_boundary, $this->_boundary->minMax(null));
	}
	
	public function testMinMax1() : void
	{
		$expected = new VersionBoundary(new Version(2, 3, 5), true);
		$this->assertEquals($this->_boundary, $this->_boundary->minMax($expected));
	}
	
	public function testMinMax2() : void
	{
		$expected = new VersionBoundary(new Version(2, 3, 3), true);
		$this->assertEquals($expected, $this->_boundary->minMax($expected));
	}
	
	public function testMinMax3() : void
	{
		$expected = new VersionBoundary(new Version(2, 3, 4), true);
		$this->assertEquals($this->_boundary, $this->_boundary->minMax($expected));
	}
	
	public function testMinMax4() : void
	{
		$expected = new VersionBoundary(new Version(2, 3, 4), true);
		$this->assertEquals($this->_boundary, $expected->minMax($this->_boundary));
	}
	
	public function testMaxMinNull() : void
	{
		$this->assertNull($this->_boundary->maxMin(null));
	}
	
	public function testMaxMin1() : void
	{
		$expected = new VersionBoundary(new Version(2, 3, 5), true);
		$this->assertEquals($expected, $this->_boundary->maxMin($expected));
	}
	
	public function testMaxMin2() : void
	{
		$expected = new VersionBoundary(new Version(2, 3, 3), true);
		$this->assertEquals($this->_boundary, $this->_boundary->maxMin($expected));
	}
	
	public function testMaxMin3() : void
	{
		$expected = new VersionBoundary(new Version(2, 3, 4), true);
		$this->assertEquals($this->_boundary, $this->_boundary->maxMin($expected));
	}
	
	public function testMaxMin4() : void
	{
		$expected = new VersionBoundary(new Version(2, 3, 4), true);
		$this->assertEquals($this->_boundary, $expected->maxMin($this->_boundary));
	}
	
	public function testMaxMaxNull() : void
	{
		$this->assertNull($this->_boundary->maxMax(null));
	}
	
	public function testMaxMax1() : void
	{
		$expected = new VersionBoundary(new Version(2, 3, 5), true);
		$this->assertEquals($expected, $this->_boundary->maxMax($expected));
	}
	
	public function testMaxMax2() : void
	{
		$expected = new VersionBoundary(new Version(2, 3, 3), true);
		$this->assertEquals($this->_boundary, $this->_boundary->maxMax($expected));
	}
	
	public function testMaxMax3() : void
	{
		$expected = new VersionBoundary(new Version(2, 3, 4), true);
		$this->assertEquals($expected, $this->_boundary->maxMax($expected));
	}
	
	public function testMaxMax4() : void
	{
		$expected = new VersionBoundary(new Version(2, 3, 4), true);
		$this->assertEquals($expected, $expected->maxMax($this->_boundary));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_boundary = new VersionBoundary(new Version(2, 3, 4), false);
	}
	
}
