<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Version\Version;
use PhpExtended\Version\VersionBoundary;
use PhpExtended\Version\VersionOperatorLowerEquals;
use PhpExtended\Version\VersionRange;
use PhpExtended\Version\VersionSegment;
use PHPUnit\Framework\TestCase;

/**
 * VersionOperatorLowerEqualsTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Version\VersionOperatorLowerEquals
 *
 * @internal
 *
 * @small
 */
class VersionOperatorLowerEqualsTest extends TestCase
{
	
	/**
	 * The operator to test.
	 * 
	 * @var VersionOperatorLowerEquals
	 */
	protected VersionOperatorLowerEquals $_operator;
	
	public function testToString() : void
	{
		$this->assertEquals('<=', $this->_operator->__toString());
	}
	
	public function testRange() : void
	{
		$base = new Version(2, 3, 4);
		$segment = new VersionSegment(new VersionBoundary(new Version(0, 0, 0), true), new VersionBoundary($base, true));
		$expected = new VersionRange([$segment]);
		$this->assertEquals($expected, $this->_operator->getRange($base));
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_operator->equals($this->_operator));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_operator = new VersionOperatorLowerEquals();
	}
	
}
