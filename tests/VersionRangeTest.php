<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Version\Version;
use PhpExtended\Version\VersionBoundary;
use PhpExtended\Version\VersionRange;
use PhpExtended\Version\VersionSegment;
use PHPUnit\Framework\TestCase;

/**
 * VersionRangeTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Version\VersionRange
 *
 * @internal
 *
 * @small
 */
class VersionRangeTest extends TestCase
{
	
	/**
	 * The range to test.
	 * 
	 * @var VersionRange
	 */
	protected VersionRange $_range;
	
	public function testToStringEmpty() : void
	{
		$this->assertEquals('∅', (new VersionRange([]))->__toString());
	}
	
	public function testToStringSegment() : void
	{
		$actual = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(2, 3, 4), true), null),
		]);
		$this->assertEquals('[2.3.4, +∞[', $actual->__toString());
	}
	
	public function testToString() : void
	{
		$this->assertEquals('(⋃ [2.3.4, 3.4.5[ , [5.6.7, +∞[ )', $this->_range->__toString());
	}
	
	public function testGetSegments() : void
	{
		$this->assertEquals([
			new VersionSegment(
				new VersionBoundary(new Version(2, 3, 4), true),
				new VersionBoundary(new Version(3, 4, 5), false),
			),
			new VersionSegment(
				new VersionBoundary(new Version(5, 6, 7), true),
				null,
			),
		], $this->_range->getSegments());
	}
	
	public function testIsEmpty() : void
	{
		$this->assertFalse($this->_range->isEmpty());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_range->equals($this->_range));
	}
	
	public function testContainsVersion() : void
	{
		$this->assertFalse($this->_range->containsVersion(new Version(1, 0, 0)));
	}
	
	public function testContainsVersion2() : void
	{
		$this->assertTrue($this->_range->containsVersion(new Version(3, 0, 0)));
	}
	
	public function testContainsVersion3() : void
	{
		$this->assertFalse($this->_range->containsVersion(new Version(4, 0, 0)));
	}
	
	public function testContainsVersion4() : void
	{
		$this->assertTrue($this->_range->containsVersion(new Version(6, 0, 0)));
	}
	
	public function testContainsBoundary() : void
	{
		$this->assertTrue($this->_range->containsBoundary(new VersionBoundary(new Version(2, 3, 4), true)));
	}
	
	public function testContainsBoundary2() : void
	{
		$this->assertFalse($this->_range->containsBoundary(new VersionBoundary(new Version(3, 4, 5), true)));
	}
	
	public function testContainsBoundary3() : void
	{
		$this->assertTrue($this->_range->containsBoundary(new VersionBoundary(new Version(5, 6, 7), false)));
	}
	
	public function testContainsBoundary4() : void
	{
		$this->assertTrue($this->_range->containsBoundary(null));
	}
	
	public function testContainsSegment() : void
	{
		foreach($this->_range->getSegments() as $segment)
		{
			$this->assertTrue($this->_range->containsSegment($segment));
		}
	}
	
	public function testContainsSegment2() : void
	{
		$this->assertFalse($this->_range->containsSegment(new VersionSegment(new VersionBoundary(new Version(1, 0, 0), true), null)));
	}
	
	public function testContainsSegment3() : void
	{
		$this->assertTrue($this->_range->containsSegment(new VersionSegment(new VersionBoundary(new Version(10, 0, 0), true), null)));
	}
	
	public function testContainsSegment4() : void
	{
		$this->assertFalse($this->_range->containsSegment(new VersionSegment(new VersionBoundary(new Version(3, 0, 0), true), new VersionBoundary(new Version(6, 0, 0), true))));
	}
	
	public function testContainsRange() : void
	{
		$this->assertTrue($this->_range->containsRange($this->_range));
	}
	
	public function testContainsRange2() : void
	{
		$range = new VersionRange([
			new VersionSegment(
				new VersionBoundary(new Version(3, 0, 0), true),
				new VersionBoundary(new Version(3, 1, 0), false),
			),
			new VersionSegment(
				new VersionBoundary(new Version(3, 2, 0), true),
				new VersionBoundary(new Version(3, 3, 0), false),
			),
			new VersionSegment(
				new VersionBoundary(new Version(8, 0, 0), true),
				null,
			),
		]);
		$this->assertTrue($this->_range->containsRange($range));
	}
	
	public function testContainsRange3() : void
	{
		$range = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(3, 0, 0), true), new VersionBoundary(new Version(3, 4, 5), true)),
		]);
		$this->assertFalse($this->_range->containsRange($range));
	}
	
	public function testUnion() : void
	{
		$range = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(1, 0, 0), true), new VersionBoundary(new Version(3, 0, 0), false)),
		]);
		$expected = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(1, 0, 0), true), new VersionBoundary(new Version(3, 4, 5), false)),
			new VersionSegment(new VersionBoundary(new Version(5, 6, 7), true), null),
		]);
		$this->assertEquals($expected, $this->_range->union($range));
	}
	
	public function testUnion2() : void
	{
		$range = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(3, 0, 0), false), new VersionBoundary(new Version(6, 0, 0), false)),
		]);
		$expected = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(2, 3, 4), true), null),
		]);
		$this->assertEquals($expected, $this->_range->union($range));
	}
	
	public function testUnion3() : void
	{
		$range = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(4, 0, 0), true), new VersionBoundary(new Version(5, 0, 0), false)),
		]);
		$expected = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(2, 3, 4), true), new VersionBoundary(new Version(3, 4, 5), false)),
			new VersionSegment(new VersionBoundary(new Version(4, 0, 0), true), new VersionBoundary(new Version(5, 0, 0), false)),
			new VersionSegment(new VersionBoundary(new Version(5, 6, 7), true), null),
		]);
		$this->assertEquals($expected, $this->_range->union($range));
	}
	
	public function testUnion4() : void
	{
		$range = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(2, 3, 4), true), new VersionBoundary(new Version(5, 6, 7), true)),
		]);
		$expected = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(2, 3, 4), true), null),
		]);
		$this->assertEquals($expected, $this->_range->union($range));
	}
	
	public function testUnion5() : void
	{
		$range = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(3, 3, 3), true), null),
		]);
		
		$expected = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(2, 3, 4), true), null),
		]);
		$this->assertEquals($expected, $this->_range->union($range));
	}
	
	public function testIntersect() : void
	{
		$this->assertEquals($this->_range, $this->_range->intersect($this->_range));
	}
	
	public function testIntersect2() : void
	{
		$range = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(3, 0, 0), true), new VersionBoundary(new Version(6, 0, 0), true)),
		]);
		$expected = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(3, 0, 0), true), new VersionBoundary(new Version(3, 4, 5), false)),
			new VersionSegment(new VersionBoundary(new Version(5, 6, 7), true), new VersionBoundary(new Version(6, 0, 0), true)),
		]);
		$this->assertEquals($expected, $this->_range->intersect($range));
	}
	
	public function testIntersect3() : void
	{
		$range = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(3, 0, 0), true), null),
		]);
		$expected = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(3, 0, 0), true), new VersionBoundary(new Version(3, 4, 5), false)),
			new VersionSegment(new VersionBoundary(new Version(5, 6, 7), true), null),
			
		]);
		$this->assertEquals($expected, $this->_range->intersect($range));
	}
	
	public function testIntersect4() : void
	{
		$range = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(1, 0, 0), true), new VersionBoundary(new Version(3, 0, 0), false)),
			new VersionSegment(new VersionBoundary(new Version(5, 0, 0), true), new VersionBoundary(new Version(6, 0, 0), false)),
		]);
		$expected = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(2, 3, 4), true), new VersionBoundary(new Version(3, 0, 0), false)),
			new VersionSegment(new VersionBoundary(new Version(5, 6, 7), true), new VersionBoundary(new Version(6, 0, 0), false)),
		]);
		$this->assertEquals($expected, $this->_range->intersect($range));
	}
	
	public function testSubtract() : void
	{
		$this->assertEquals(new VersionRange([]), $this->_range->subtract($this->_range));
	}
	
	public function testSubtract2() : void
	{
		$range = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(0, 0, 0), true), new VersionBoundary(new Version(1, 0, 0), true)),
		]);
		$this->assertEquals($this->_range, $this->_range->subtract($range));
	}
	
	public function testSubtract3() : void
	{
		$range = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(6, 0, 0), true), new VersionBoundary(new Version(7, 0, 0), false)),
		]);
		$expected = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(2, 3, 4), true), new VersionBoundary(new Version(3, 4, 5), false)),
			new VersionSegment(new VersionBoundary(new Version(5, 6, 7), true), new VersionBoundary(new Version(6, 0, 0), false)),
			new VersionSegment(new VersionBoundary(new Version(7, 0, 0), true), null),
		]);
		$this->assertEquals($expected, $this->_range->subtract($range));
	}
	
	public function testSubtract4() : void
	{
		$range = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(3, 0, 0), true), null),
		]);
		$expected = new VersionRange([
			new VersionSegment(new VersionBoundary(new Version(2, 3, 4), true), new VersionBoundary(new Version(3, 0, 0), false)),
		]);
		$this->assertEquals($expected, $this->_range->subtract($range));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_range = new VersionRange([
			new VersionSegment(
				new VersionBoundary(new Version(2, 3, 4), true),
				new VersionBoundary(new Version(3, 4, 5), false),
			),
			new VersionSegment(
				new VersionBoundary(new Version(5, 6, 7), true),
				null,
			),
		]);
	}
	
}
