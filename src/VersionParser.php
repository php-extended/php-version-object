<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Version;

use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Parser\AbstractParserLexer;

/**
 * VersionParser class file.
 * 
 * This class is a simple implementation of the VersionParserInterface.
 * 
 * @author Anastaszor
 * @extends AbstractParserLexer<VersionInterface>
 */
class VersionParser extends AbstractParserLexer implements VersionParserInterface
{
	
	public const L_NUM = 1;
	public const L_SEP = 2;
	public const L_ALPHA = 3;
	
	/**
	 * Builds a new UuidParser with the given lexer config.
	 */
	public function __construct()
	{
		parent::__construct(VersionInterface::class);
		
		$this->_config->addMappings('.-+_', self::L_SEP);
		$this->_config->addMappings(LexerInterface::CLASS_DIGIT, self::L_NUM);
		$this->_config->addMappings(LexerInterface::CLASS_ALPHA, self::L_ALPHA);
		
		$this->_config->addMerging(self::L_NUM, self::L_NUM, self::L_NUM);
		$this->_config->addMerging(self::L_ALPHA, self::L_ALPHA, self::L_ALPHA);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionParserInterface::parse()
	 */
	public function parse(?string $data) : VersionInterface
	{
		return parent::parse($data); // interface compatibility
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\AbstractParserLexer::parseLexer()
	 */
	public function parseLexer(LexerInterface $lexer) : VersionInterface
	{
		$major = null;
		$minor = null;
		$patch = null;
		$label = '';
		
		foreach($lexer as $lexeme)
		{
			if($lexeme->getCode() === self::L_NUM)
			{
				if(null === $major)
				{
					$major = (int) $lexeme->getData();
					continue;
				}
				
				if(null === $minor)
				{
					$minor = (int) $lexeme->getData();
					continue;
				}
				
				if(null === $patch)
				{
					$patch = (int) $lexeme->getData();
					continue;
				}
				
				$label .= $lexeme->getData();
				
				continue;
			}
			
			if(null === $patch)
			{
				continue; // just ignore
			}
			
			$label .= $lexeme->getData();
		}
		
		return new Version((int) $major, (int) $minor, (int) $patch, empty($label) ? null : \trim($label, '.-+_'));
	}
	
}
