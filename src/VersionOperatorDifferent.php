<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Version;

/**
 * VersionOperatorDifferent class file.
 * 
 * The different operator allows only the version that are not equal to the
 * specified version number.
 * 
 * @author Anastaszor
 */
class VersionOperatorDifferent implements VersionOperatorInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return '!=';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionOperatorInterface::getRange()
	 */
	public function getRange(VersionInterface $base) : VersionRangeInterface
	{
		$boundary = new VersionBoundary($base, false);
		
		return new VersionRange([
			new VersionSegment(
				new VersionBoundary(new Version(0, 0, 0), true),
				$boundary,
			),
			new VersionSegment(
				$boundary,
				null,
			),
		]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionOperatorInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof self;
	}
	
}
