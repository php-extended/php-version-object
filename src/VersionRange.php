<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Version;

/**
 * VersionRangeSimple class file.
 * 
 * This class represents a convexe volume of version in version space.
 * 
 * @author Anastaszor
 */
class VersionRange implements VersionRangeInterface
{
	
	/**
	 * The segments of this range. This list is an ordered list of disjoint
	 * segments that are ordered from lower to higher. As disjoint segments,
	 * their union form the full range.
	 * 
	 * @var array<integer, VersionSegmentInterface>
	 */
	protected array $_segments = [];
	
	/**
	 * Builds a new VersionRangeSimple with the given segments.
	 * 
	 * @param array<integer, ?VersionSegmentInterface> $segments
	 */
	public function __construct(array $segments)
	{
		$this->_segments = $this->split($this->sort($this->filter($segments)));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		if($this->isEmpty())
		{
			return '∅';
		}
		
		$segmentsstr = [];
		
		foreach($this->_segments as $segment)
		{
			$segmentsstr[] = $segment->__toString();
		}
		
		if(1 === \count($segmentsstr))
		{
			return \implode('', $segmentsstr);
		}
		
		return '(⋃ '.\implode(' , ', $segmentsstr).' )';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionRangeInterface::getSegments()
	 */
	public function getSegments() : array
	{
		return $this->_segments;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionRangeInterface::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return 0 === \count($this->_segments);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionRangeInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof VersionRangeInterface
			&& $this->containsRange($object)
			&& $object->containsRange($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionRangeInterface::containsVersion()
	 */
	public function containsVersion(VersionInterface $version) : bool
	{
		foreach($this->_segments as $segment)
		{
			if($segment->containsVersion($version))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionRangeInterface::containsBoundary()
	 */
	public function containsBoundary(?VersionBoundaryInterface $boundary) : bool
	{
		foreach($this->_segments as $segment)
		{
			if($segment->containsBoundary($boundary))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionRangeInterface::containsSegment()
	 */
	public function containsSegment(VersionSegmentInterface $interval) : bool
	{
		foreach($this->_segments as $segment)
		{
			if($segment->containsSegment($interval))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionRangeInterface::containsRange()
	 */
	public function containsRange(VersionRangeInterface $range) : bool
	{
		foreach($range->getSegments() as $segment)
		{
			if(!$this->containsSegment($segment))
			{
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionRangeInterface::union()
	 */
	public function union(VersionRangeInterface $range) : VersionRangeInterface
	{
		$segments = \array_merge($this->_segments, $range->getSegments());
		
		return new VersionRange($segments); // sort and split are done in ctor
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionRangeInterface::intersect()
	 */
	public function intersect(VersionRangeInterface $range) : VersionRangeInterface
	{
		$ranges = [];
		
		foreach($range->getSegments() as $segment)
		{
			$intersected = $this->intersectSegment($this->_segments, $segment);
			
			foreach($intersected as $range)
			{
				$ranges[] = $range;
			}
		}
		
		return new self($ranges);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionRangeInterface::subtract()
	 */
	public function subtract(VersionRangeInterface $range) : VersionRangeInterface
	{
		$ranges = $this->_segments;
		
		foreach($range->getSegments() as $segment)
		{
			$ranges = $this->subtractSegment($ranges, $segment);
		}
		
		return new self($ranges);
	}
	
	/**
	 * Filters out the empty segments.
	 * 
	 * @param array<integer, ?VersionSegmentInterface> $segments
	 * @return array<integer, VersionSegmentInterface>
	 */
	protected function filter(array $segments) : array
	{
		$list = [];
		
		foreach($segments as $segment)
		{
			if(null !== $segment && !$segment->isEmpty())
			{
				$list[] = $segment;
			}
		}
		
		return $list;
	}
	
	/**
	 * Sort the segments.
	 * 
	 * @param array<integer, VersionSegmentInterface> $segments
	 * @return array<integer, VersionSegmentInterface>
	 */
	protected function sort(array $segments) : array
	{
		\usort($segments, function(VersionSegmentInterface $seg1, VersionSegmentInterface $seg2)
		{
			if($seg1->getLowerBound()->getBaseVersion()->isStrictlyGreaterThan($seg2->getLowerBound()->getBaseVersion()))
			{
				return 1;
			}
			if($seg1->getLowerBound()->getBaseVersion()->isStrictlyLowerThan($seg2->getLowerBound()->getBaseVersion()))
			{
				return -1;
			}
			
			return 0;
		});
		
		return $segments;
	}
	
	/**
	 * Separates the joined segments.
	 * 
	 * @param array<integer, VersionSegmentInterface> $segments
	 * @return array<integer, VersionSegmentInterface>
	 */
	protected function split(array $segments) : array
	{
		$list = [];
		/** @var ?VersionSegmentInterface $last */
		$last = null;
		
		foreach($segments as $segment)
		{
			/** @var VersionSegmentInterface $segment */
			if(null === $last)
			{
				$last = $segment;
				continue;
			}
			
			$upper = $last->getUpperBound();
			if(null === $upper)
			{
				break;
			}
			
			if($upper->getBaseVersion()->isGreaterThanOrEquals($segment->getLowerBound()->getBaseVersion()))
			{
				// merge them
				$last = new VersionSegment($last->getLowerBound(), $segment->getUpperBound());
				continue;
			}
			
			$list[] = $last;
			$last = $segment;
		}
		
		if(null !== $last)
		{
			$list[] = $last;
		}
		
		return $list;
	}
	
	/**
	 * Intersects the given segment from all the other segments.
	 * 
	 * @param array<integer, VersionSegmentInterface> $ranges
	 * @param VersionSegmentInterface $segment
	 * @return array<integer, VersionSegmentInterface>
	 */
	protected function intersectSegment(array $ranges, VersionSegmentInterface $segment)
	{
		$segments = [];
		
		foreach($ranges as $range)
		{
			$intersect = $range->intersect($segment);
			
			foreach($intersect->getSegments() as $intersected)
			{
				if(!$intersected->isEmpty())
				{
					$segments[] = $intersected;
				}
			}
		}
		
		return $segments;
	}
	
	/**
	 * Subtract the given segment from the other segments.
	 * 
	 * @param array<integer, VersionSegmentInterface> $ranges
	 * @param VersionSegmentInterface $segment
	 * @return array<integer, VersionSegmentInterface>
	 */
	protected function subtractSegment(array $ranges, VersionSegmentInterface $segment)
	{
		$segments = [];
		
		foreach($ranges as $range)
		{
			$subtract = $range->subtract($segment);
			
			foreach($subtract->getSegments() as $subtracted)
			{
				if(!$subtracted->isEmpty())
				{
					$segments[] = $subtracted;
				}
			}
		}
		
		return $segments;
	}
	
}
