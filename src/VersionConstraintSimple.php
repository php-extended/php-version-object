<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Version;

/**
 * VersionConstraintSimple class file.
 * 
 * This class represents a simple constraint that is built from an operator
 * and a version number.
 * 
 * @author Anastaszor
 */
class VersionConstraintSimple implements VersionConstraintInterface
{
	
	/**
	 * The operator for this constraint.
	 * 
	 * @var VersionOperatorInterface
	 */
	protected VersionOperatorInterface $_operator;
	
	/**
	 * The version for this constraint.
	 * 
	 * @var VersionInterface
	 */
	protected VersionInterface $_version;
	
	/**
	 * Builds a new VersionConstraintSimple with the given operator and version.
	 * 
	 * @param VersionOperatorInterface $operator
	 * @param VersionInterface $version
	 */
	public function __construct(VersionOperatorInterface $operator, VersionInterface $version)
	{
		$this->_operator = $operator;
		$this->_version = $version;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_operator->__toString().$this->_version->__toString();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionConstraintInterface::getOperator()
	 */
	public function getOperator() : VersionOperatorInterface
	{
		return $this->_operator;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionConstraintInterface::getEffectiveRange()
	 */
	public function getRange() : VersionRangeInterface
	{
		return $this->_operator->getRange($this->_version);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionConstraintInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof VersionConstraintInterface
			&& $this->getRange()->equals($object->getRange());
	}
	
}
