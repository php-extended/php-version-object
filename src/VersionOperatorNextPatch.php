<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Version;

/**
 * VersionOperatorNextPatch class file.
 * 
 * The next patch operator allows any version that is above or equal to the
 * specified version up to next patch, not included.
 * 
 * @author Anastaszor
 */
class VersionOperatorNextPatch implements VersionOperatorInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return '~';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionOperatorInterface::getRange()
	 */
	public function getRange(VersionInterface $base) : VersionRangeInterface
	{
		return new VersionRange([
			new VersionSegment(
				new VersionBoundary($base, true),
				new VersionBoundary($base->incrementPatch(), false),
			),
		]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionOperatorInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof self;
	}
	
}
