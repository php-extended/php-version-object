<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Version;

/**
 * VersionSegment class file.
 * 
 * This class is a simple implementation of the VersionSegmentInterface.
 * 
 * @author Anastaszor
 */
class VersionSegment implements VersionSegmentInterface
{
	
	/**
	 * The lower boundary.
	 *
	 * @var VersionBoundaryInterface
	 */
	protected VersionBoundaryInterface $_lower;
	
	/**
	 * The upper boundary.
	 *
	 * @var ?VersionBoundaryInterface
	 */
	protected ?VersionBoundaryInterface $_upper;
	
	/**
	 * Builds a new VersionRangeSimple with the given lower and upper boundaries.
	 *
	 * @param VersionBoundaryInterface $lower
	 * @param VersionBoundaryInterface $upper
	 */
	public function __construct(VersionBoundaryInterface $lower, ?VersionBoundaryInterface $upper)
	{
		$this->_lower = $lower;
		$this->_upper = $upper;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		$str = ($this->_lower->isIncluded() ? '[' : ']').$this->_lower->getBaseVersion()->__toString().', ';
		
		if(null === $this->_upper)
		{
			return $str.'+∞[';
		}
		
		return $str.$this->_upper->getBaseVersion()->__toString().($this->_upper->isIncluded() ? ']' : '[');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionSegmentInterface::getLowerBound()
	 */
	public function getLowerBound() : VersionBoundaryInterface
	{
		return $this->_lower;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionSegmentInterface::getUpperBound()
	 */
	public function getUpperBound() : ?VersionBoundaryInterface
	{
		return $this->_upper;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionSegmentInterface::isEmpty()
	 */
	public function isEmpty() : bool
	{
		if(null === $this->_upper)
		{
			return false;
		}
		
		if($this->_lower->getBaseVersion()->isStrictlyGreaterThan($this->_upper->getBaseVersion()))
		{
			return true;
		}
		
		if($this->_lower->getBaseVersion()->isStrictlyLowerThan($this->_upper->getBaseVersion()))
		{
			return false;
		}
		
		// if boundaries' versions are equal, both need to include it
		return !$this->_lower->isIncluded() || !$this->_upper->isIncluded();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionSegmentInterface::equals()
	 */
	public function equals($object) : bool
	{
		$upperbound = $this->getUpperBound();
		
		return $object instanceof VersionSegmentInterface
			&& $this->getLowerBound()->equals($object->getLowerBound())
			&& (null === $upperbound || $upperbound->equals($object->getUpperBound()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionSegmentInterface::containsVersion()
	 */
	public function containsVersion(VersionInterface $version) : bool
	{
		$lower = $this->getLowerBound();
		
		$lowerOk = $lower->isIncluded()
			? $lower->getBaseVersion()->isLowerThanOrEquals($version)
			: $lower->getBaseVersion()->isStrictlyLowerThan($version);
		
		if(!$lowerOk)
		{
			return false;
		}
		
		$upper = $this->getUpperBound();
		if(null === $upper)
		{
			return true;
		}
		
		return $upper->isIncluded()
			? $upper->getBaseVersion()->isGreaterThanOrEquals($version)
			: $upper->getBaseVersion()->isStrictlyGreaterThan($version);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionSegmentInterface::containsBoundary()
	 */
	public function containsBoundary(?VersionBoundaryInterface $boundary) : bool
	{
		if(null === $boundary)
		{
			return null === $this->_upper;
		}
		
		if($boundary->isIncluded())
		{
			return $this->containsVersion($boundary->getBaseVersion());
		}
		
		$lower = $this->getLowerBound();
		if(!$lower->getBaseVersion()->isLowerThanOrEquals($boundary->getBaseVersion()))
		{
			return false;
		}
		
		$upper = $this->getUpperBound();
		if(null === $upper)
		{
			return true;
		}
		
		return $upper->getBaseVersion()->isGreaterThanOrEquals($boundary->getBaseVersion());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionSegmentInterface::containsSegment()
	 */
	public function containsSegment(VersionSegmentInterface $interval) : bool
	{
		return $this->containsBoundary($interval->getLowerBound())
			&& $this->containsBoundary($interval->getUpperBound());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionSegmentInterface::union()
	 */
	public function union(VersionSegmentInterface $interval) : VersionRangeInterface
	{
		if($this->containsBoundary($interval->getLowerBound()) || $this->containsBoundary($interval->getUpperBound()))
		{
			return new VersionRange([
				new self(
					$this->_lower->minMin($interval->getLowerBound()),
					null === $this->_upper 
						? $interval->getUpperBound() 
						: $this->_upper->maxMax($interval->getUpperBound()),
				),
			]);
		}
		
		// else they are disjoint
		if($this->_lower->getBaseVersion()->isLowerThanOrEquals($interval->getLowerBound()->getBaseVersion()))
		{
			return new VersionRange([$this, $interval]);
		}
		
		return new VersionRange([$interval, $this]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionSegmentInterface::intersect()
	 */
	public function intersect(VersionSegmentInterface $interval) : VersionRangeInterface
	{
		if($this->containsSegment($interval)) // [{ }]
		{
			return new VersionRange([$interval]);
		}
		
		if($interval->containsSegment($this)) // {[ ]}
		{
			return new VersionRange([$this]);
		}
		
		if($this->containsBoundary($interval->getLowerBound()) || $interval->containsBoundary($this->getLowerBound()))
		{
			// [ {] } or { [} ]
			$lower = $this->_lower->maxMin($interval->getLowerBound());
			$upper = null === $this->_upper
				? $interval->getUpperBound()
				: $this->_upper->minMax($interval->getUpperBound());
			
			if(null !== $lower)
			{
				return new VersionRange([new self($lower, $upper)]);
			}
		}
		
		// [] {}
		return new VersionRange([]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionSegmentInterface::subtract()
	 */
	public function subtract(VersionSegmentInterface $interval) : VersionRangeInterface
	{
		$arrs = [];
		
		$before = new self(
			$this->_lower,
			(new VersionBoundary($interval->getLowerBound()->getBaseVersion(), !$interval->getLowerBound()->isIncluded()))->minMax($this->_upper),
		);
		
		if(!$before->isEmpty())
		{
			$arrs[] = $before;
		}
		
		$otherUpper = $interval->getUpperBound();
		if(null !== $otherUpper)
		{
			$lower = (new VersionBoundary($otherUpper->getBaseVersion(), !$otherUpper->isIncluded()))->maxMin($this->_lower);
			if(null !== $lower)
			{
				$after = new self($lower, $this->getUpperBound());
				
				if(!$after->isEmpty())
				{
					$arrs[] = $after;
				}
			}
		}
		
		return new VersionRange($arrs);
	}
	
}
