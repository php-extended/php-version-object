<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Version;

/**
 * VersionOperatorHigherEquals class file.
 * 
 * The higher or equals operator allows any version that is above or equal to
 * the specified version number, regardless of the gap between both.
 * 
 * @author Anastaszor
 */
class VersionOperatorHigherEquals implements VersionOperatorInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return '>=';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionOperatorInterface::getRange()
	 */
	public function getRange(VersionInterface $base) : VersionRangeInterface
	{
		return new VersionRange([
			new VersionSegment(
				new VersionBoundary($base, true),
				null,
			),
		]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionOperatorInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof self;
	}
	
}
