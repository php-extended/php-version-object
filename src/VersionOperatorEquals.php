<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Version;

/**
 * VersionOperatorEquals class file.
 * 
 * The equals operator allows only the version that is equals to the specified
 * version number.
 * 
 * @author Anastaszor
 */
class VersionOperatorEquals implements VersionOperatorInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return '';
	}
	
	/**
	 * Gets the range that is given by this operator applied to the given base
	 * version number.
	 *
	 * @param VersionInterface $base
	 * @return VersionRangeInterface
	 */
	public function getRange(VersionInterface $base) : VersionRangeInterface
	{
		$boundary = new VersionBoundary($base, true);
		
		return new VersionRange([
			new VersionSegment(
				$boundary,
				$boundary,
			),
		]);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionOperatorInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof self;
	}
	
}
