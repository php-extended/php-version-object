<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Version;

/**
 * Version class file.
 * 
 * This class is a simple implementation of the VersionInterface.
 * 
 * @author Anastaszor
 */
class Version implements VersionInterface
{
	
	/**
	 * The major version number.
	 * 
	 * @var integer
	 */
	protected int $_major = 0;
	
	/**
	 * The minor version number.
	 * 
	 * @var integer
	 */
	protected int $_minor = 0;
	
	/**
	 * The patch version number.
	 * 
	 * @var integer
	 */
	protected int $_patch = 0;
	
	/**
	 * The string label after the patch version number.
	 * 
	 * @var ?string
	 */
	protected ?string $_label = null;
	
	/**
	 * Builds a new Version based on its components.
	 * 
	 * @param integer $major
	 * @param integer $minor
	 * @param integer $patch
	 * @param string $label
	 */
	public function __construct(int $major, int $minor, int $patch, ?string $label = null)
	{
		$this->_major = \max(0, $major);
		$this->_minor = \max(0, $minor);
		$this->_patch = \max(0, $patch);
		$this->_label = $label;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return ((string) $this->_major)
			.'.'.((string) $this->_minor)
			.'.'.((string) $this->_patch)
			.(\strlen((string) $this->_label) > 0 ? '-'.((string) $this->_label) : '');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionInterface::getMajor()
	 */
	public function getMajor() : int
	{
		return $this->_major;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionInterface::getMinor()
	 */
	public function getMinor() : int
	{
		return $this->_minor;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionInterface::getPatch()
	 */
	public function getPatch() : int
	{
		return $this->_patch;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionInterface::getLabel()
	 */
	public function getLabel() : string
	{
		return (string) $this->_label;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionInterface::toInteger()
	 */
	public function toInteger() : int
	{
		return $this->_major * 10000 + $this->_minor * 100 + $this->_patch;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionInterface::toArray()
	 */
	public function toArray() : array
	{
		return [
			(string) $this->_major,
			(string) $this->_minor,
			(string) $this->_patch,
			(string) $this->_label,
		];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionInterface::isDeveloppement()
	 */
	public function isDeveloppement() : bool
	{
		return 0 === $this->_major;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionInterface::incrementMajor()
	 */
	public function incrementMajor() : VersionInterface
	{
		return new self($this->_major + 1, 0, 0);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionInterface::incrementMinor()
	 */
	public function incrementMinor() : VersionInterface
	{
		return new self($this->_major, $this->_minor + 1, 0);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionInterface::incrementPatch()
	 */
	public function incrementPatch() : VersionInterface
	{
		return new self($this->_major, $this->_minor, $this->_patch + 1);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionInterface::getLabelSpecified()
	 */
	public function setLabel(string $label) : VersionInterface
	{
		return new self($this->_major, $this->_minor, $this->_patch, $label);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof VersionInterface
			&& $object->getMajor() === $this->getMajor()
			&& $object->getMinor() === $this->getMinor()
			&& $object->getPatch() === $this->getPatch()
			&& $object->getLabel() === $this->getLabel();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionInterface::min()
	 */
	public function min(VersionInterface $other) : VersionInterface
	{
		if($this->isLowerThanOrEquals($other))
		{
			return $this;
		}
		
		return $other;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionInterface::max()
	 */
	public function max(VersionInterface $other) : VersionInterface
	{
		if($this->isGreaterThanOrEquals($other))
		{
			return $this;
		}
		
		return $other;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionInterface::isStrictlyGreaterThan()
	 */
	public function isStrictlyGreaterThan(VersionInterface $other) : bool
	{
		if($this->getMajor() > $other->getMajor())
		{
			return true;
		}
		if($this->getMajor() < $other->getMajor())
		{
			return false;
		}
		
		if($this->getMinor() > $other->getMinor())
		{
			return true;
		}
		if($this->getMinor() < $other->getMinor())
		{
			return false;
		}
		
		if($this->getPatch() > $other->getPatch())
		{
			return true;
		}
		if($this->getPatch() < $other->getPatch())
		{
			return false;
		}
		
		foreach(['rc', 'beta', 'alpha'] as $label)
		{
			$choses = $this->isStrictlyGreaterFromLabel($this, $other, $label);
			if(null !== $choses)
			{
				return $choses;
			}
		}
		
		return 0 < \strcasecmp($this->getLabel(), $other->getLabel());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionInterface::isGreaterThanOrEquals()
	 */
	public function isGreaterThanOrEquals(VersionInterface $other) : bool
	{
		return $this->equals($other) || $this->isStrictlyGreaterThan($other);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionInterface::isStrictlyLowerThan()
	 */
	public function isStrictlyLowerThan(VersionInterface $other) : bool
	{
		return $other->isStrictlyGreaterThan($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionInterface::isLowerThanOrEquals()
	 */
	public function isLowerThanOrEquals(VersionInterface $other) : bool
	{
		return $other->isGreaterThanOrEquals($this);
	}
	
	/**
	 * Gets whether the given version contains the given label at the beginning
	 * of its label string part.
	 * 
	 * @param VersionInterface $version
	 * @param string $label LOWERCASE label value
	 * @return boolean
	 */
	protected function hasLabel(VersionInterface $version, string $label) : bool
	{
		return false !== \mb_stripos($version->getLabel(), $label);
	}
	
	/**
	 * Gets whether the discriminant makes a choice between the two versions.
	 * 
	 * @param VersionInterface $version1
	 * @param VersionInterface $version2
	 * @param string $discriminant
	 * @return ?boolean
	 */
	protected function isStrictlyGreaterFromLabel(VersionInterface $version1, VersionInterface $version2, string $discriminant) : ?bool
	{
		$thisHas = $this->hasLabel($version1, $discriminant);
		$otherHas = $this->hasLabel($version2, $discriminant);
		
		if($thisHas && $otherHas)
		{
			$diff = \strcasecmp($version1->getLabel(), $version2->getLabel());
			if(0 < $diff)
			{
				return true;
			}
			
			if(0 > $diff)
			{
				return false;
			}
			
			return null;
		}
		
		if($thisHas)
		{
			return true;
		}
		
		if($otherHas)
		{
			return false;
		}
		
		return null;
	}
	
}
