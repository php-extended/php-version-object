<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Version;

/**
 * VersionConstraintUnion class file.
 * 
 * This class represents an union between two version constraints.
 * 
 * @author Anastaszor
 */
class VersionConstraintUnion implements VersionConstraintInterface
{
	
	/**
	 * The left version constraint.
	 * 
	 * @var VersionConstraintInterface
	 */
	protected VersionConstraintInterface $_left;
	
	/**
	 * The right version constraint.
	 * 
	 * @var VersionConstraintInterface
	 */
	protected VersionConstraintInterface $_right;
	
	/**
	 * Builds a new VersionConstraintUnion.
	 * 
	 * @param VersionConstraintInterface $left
	 * @param VersionConstraintInterface $right
	 */
	public function __construct(VersionConstraintInterface $left, VersionConstraintInterface $right)
	{
		$this->_left = $left;
		$this->_right = $right;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return '( '.$this->_left->__toString().' || '.$this->_right->__toString().' )';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionConstraintInterface::getOperator()
	 */
	public function getOperator() : VersionOperatorInterface
	{
		return $this->_left->getOperator();
	}
	
	/**
	 * Gets the range this version constraint defines with its base version
	 * and its operator.
	 *
	 * @return VersionRangeInterface
	 */
	public function getRange() : VersionRangeInterface
	{
		return $this->_left->getRange()->union($this->_right->getRange());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionConstraintInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof VersionConstraintInterface
			&& $this->getRange()->equals($object->getRange());
	}
	
}
