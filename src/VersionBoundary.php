<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Version;

/**
 * VersionBoundary class file.
 * 
 * This class is a simple implementation of the VersionBoundaryInterface.
 * 
 * @author Anastaszor
 */
class VersionBoundary implements VersionBoundaryInterface
{
	
	/**
	 * The base version.
	 * 
	 * @var VersionInterface
	 */
	protected VersionInterface $_base;
	
	/**
	 * Whether the base is included in the boundary.
	 * 
	 * @var boolean
	 */
	protected bool $_included = false;
	
	/**
	 * Builds a new VersionBoundary with the given version and included flag.
	 * 
	 * @param VersionInterface $version
	 * @param boolean $included
	 */
	public function __construct(VersionInterface $version, bool $included)
	{
		$this->_base = $version;
		$this->_included = $included;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return ($this->_included ? '' : '!').$this->_base->__toString();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionBoundaryInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $object instanceof VersionBoundaryInterface
			&& $this->isIncluded() === $object->isIncluded()
			&& $this->getBaseVersion()->equals($object->getBaseVersion());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionBoundaryInterface::getBaseVersion()
	 */
	public function getBaseVersion() : VersionInterface
	{
		return $this->_base;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionBoundaryInterface::isIncluded()
	 */
	public function isIncluded() : bool
	{
		return $this->_included;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionBoundaryInterface::minMin()
	 */
	public function minMin(?VersionBoundaryInterface $other) : VersionBoundaryInterface
	{
		if(null === $other)
		{
			return $this;
		}
		
		if($this->getBaseVersion()->isStrictlyLowerThan($other->getBaseVersion()))
		{
			return $this;
		}
		
		if($this->getBaseVersion()->isStrictlyGreaterThan($other->getBaseVersion()))
		{
			return $other;
		}
		
		if($this->isIncluded())
		{
			return $this;
		}
		
		return $other;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionBoundaryInterface::minMax()
	 */
	public function minMax(?VersionBoundaryInterface $other) : VersionBoundaryInterface
	{
		if(null === $other)
		{
			return $this;
		}
		
		if($this->getBaseVersion()->isStrictlyLowerThan($other->getBaseVersion()))
		{
			return $this;
		}
		
		if($this->getBaseVersion()->isStrictlyGreaterThan($other->getBaseVersion()))
		{
			return $other;
		}
		
		if($this->isIncluded())
		{
			return $other;
		}
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionBoundaryInterface::maxMin()
	 */
	public function maxMin(?VersionBoundaryInterface $other) : ?VersionBoundaryInterface
	{
		if(null === $other)
		{
			return $other;
		}
		
		if($this->getBaseVersion()->isStrictlyLowerThan($other->getBaseVersion()))
		{
			return $other;
		}
		
		if($this->getBaseVersion()->isStrictlyGreaterThan($other->getBaseVersion()))
		{
			return $this;
		}
		
		if($this->isIncluded())
		{
			return $other;
		}
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Version\VersionBoundaryInterface::maxMax()
	 */
	public function maxMax(?VersionBoundaryInterface $other) : ?VersionBoundaryInterface
	{
		if(null === $other)
		{
			return $other;
		}
		
		if($this->getBaseVersion()->isStrictlyLowerThan($other->getBaseVersion()))
		{
			return $other;
		}
		
		if($this->getBaseVersion()->isStrictlyGreaterThan($other->getBaseVersion()))
		{
			return $this;
		}
		
		if($this->isIncluded())
		{
			return $this;
		}
		
		return $other;
	}
	
}
