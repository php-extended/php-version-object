<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-version-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Version;

use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Parser\AbstractParserLexer;
use PhpExtended\Parser\ParseException;
use PhpExtended\Parser\ParseThrowable;

/**
 * VersionConstraintParser class file.
 * 
 * This clas is a simple implementation of the VersionConstraintParserInterface.
 * 
 * @author Anastaszor
 * @extends AbstractParserLexer<VersionConstraintInterface>
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class VersionConstraintParser extends AbstractParserLexer implements VersionConstraintParserInterface
{
	
	public const L_BLOB = 1; // alphanum .-*
	public const L_SPACE = 2; // sp
	public const L_LOWER = 3; // <
	public const L_UPPER = 4; // >
	public const L_EXCL = 5; // !
	public const L_EQLS = 6; // =
	public const L_TILDE = 7; // ~
	public const L_CARET = 8; // ^
	public const L_VBAR = 9; // |
	public const L_ESPER = 10; // &
	public const L_OPPAR = 11; // (
	public const L_CLPAR = 12; // )
	public const L_COMMA = 13; // ,
	public const OP_LEQ = 21; // <=
	public const OP_UEQ = 22; // >=
	public const OP_OR_ = 23; // ||
	public const OP_AND = 24; // &&
	public const OP_DIF = 25; // != OR <>
	
	/**
	 * The version parser.
	 * 
	 * @var VersionParserInterface
	 */
	protected VersionParserInterface $_versionParser;
	
	/**
	 * Builds a new UuidParser with the given lexer config.
	 */
	public function __construct()
	{
		parent::__construct(VersionConstraintInterface::class);
		$this->_versionParser = new VersionParser();
		
		$this->_config->addMappings(LexerInterface::CLASS_ALNUM, self::L_BLOB);
		$this->_config->addMappings('.-*', self::L_BLOB);
		$this->_config->addMappings(' ', self::L_SPACE);
		$this->_config->addMappings('<', self::L_LOWER);
		$this->_config->addMappings('>', self::L_UPPER);
		$this->_config->addMappings('!', self::L_EXCL);
		$this->_config->addMappings('=', self::L_EQLS);
		$this->_config->addMappings('~', self::L_TILDE);
		$this->_config->addMappings('^', self::L_CARET);
		$this->_config->addMappings('|', self::L_VBAR);
		$this->_config->addMappings('&', self::L_ESPER);
		$this->_config->addMappings('(', self::L_OPPAR);
		$this->_config->addMappings(')', self::L_CLPAR);
		$this->_config->addMappings(',', self::L_COMMA);
		
		$this->_config->addMerging(self::L_BLOB, self::L_BLOB, self::L_BLOB);
		$this->_config->addMerging(self::L_SPACE, self::L_SPACE, self::L_SPACE);
		
		$this->_config->addMerging(self::L_SPACE, self::L_LOWER, self::L_LOWER);
		$this->_config->addMerging(self::L_LOWER, self::L_SPACE, self::L_LOWER);
		$this->_config->addMerging(self::L_LOWER, self::L_EQLS, self::OP_LEQ);
		$this->_config->addMerging(self::OP_LEQ, self::L_SPACE, self::OP_LEQ);
		
		$this->_config->addMerging(self::L_SPACE, self::L_UPPER, self::L_UPPER);
		$this->_config->addMerging(self::L_UPPER, self::L_SPACE, self::L_UPPER);
		$this->_config->addMerging(self::L_UPPER, self::L_EQLS, self::OP_UEQ);
		$this->_config->addMerging(self::OP_UEQ, self::L_SPACE, self::OP_UEQ);
		
		$this->_config->addMerging(self::L_SPACE, self::L_EXCL, self::L_EXCL);
		$this->_config->addMerging(self::L_EXCL, self::L_SPACE, self::L_EXCL);
		
		$this->_config->addMerging(self::L_SPACE, self::L_EQLS, self::L_EQLS);
		$this->_config->addMerging(self::L_EQLS, self::L_SPACE, self::L_EQLS);
		
		$this->_config->addMerging(self::L_SPACE, self::L_TILDE, self::L_TILDE);
		$this->_config->addMerging(self::L_TILDE, self::L_SPACE, self::L_TILDE);
		
		$this->_config->addMerging(self::L_SPACE, self::L_CARET, self::L_CARET);
		$this->_config->addMerging(self::L_CARET, self::L_SPACE, self::L_CARET);
		
		$this->_config->addMerging(self::L_SPACE, self::L_VBAR, self::L_VBAR);
		$this->_config->addMerging(self::L_VBAR, self::L_SPACE, self::L_VBAR);
		$this->_config->addMerging(self::L_VBAR, self::L_VBAR, self::OP_OR_);
		$this->_config->addMerging(self::OP_OR_, self::L_SPACE, self::OP_OR_);
		
		$this->_config->addMerging(self::L_SPACE, self::L_ESPER, self::L_ESPER);
		$this->_config->addMerging(self::L_ESPER, self::L_SPACE, self::L_ESPER);
		$this->_config->addMerging(self::L_ESPER, self::L_ESPER, self::OP_AND);
		$this->_config->addMerging(self::OP_AND, self::L_SPACE, self::OP_AND);
		
		$this->_config->addMerging(self::L_SPACE, self::L_OPPAR, self::L_OPPAR);
		$this->_config->addMerging(self::L_OPPAR, self::L_SPACE, self::L_OPPAR);
		
		$this->_config->addMerging(self::L_SPACE, self::L_CLPAR, self::L_CLPAR);
		$this->_config->addMerging(self::L_CLPAR, self::L_SPACE, self::L_CLPAR);
		
		$this->_config->addMerging(self::L_EXCL, self::L_EQLS, self::OP_DIF);
		$this->_config->addMerging(self::L_LOWER, self::L_UPPER, self::OP_DIF);
		$this->_config->addMerging(self::OP_DIF, self::L_SPACE, self::OP_DIF);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : VersionConstraintInterface
	{
		$lexer = $this->_factory->createFromString((string) $data, $this->_config);
		
		try
		{
			return $this->parseLexer($lexer);
		}
		catch(ParseThrowable $e)
		{
			return new VersionConstraintSimple(new VersionOperatorEquals(), new Version(0, 0, 0));
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\AbstractParserLexer::parseLexer()
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function parseLexer(LexerInterface $lexer, bool $rewind = true) : VersionConstraintInterface
	{
		if($rewind)
		{
			$lexer->rewind();
		}
		
		$lastConstraint = null;
		$conjunction = true; // if true AND else OR
		$currentOp = new VersionOperatorEquals();
		
		while($lexer->valid())
		{
			$token = $lexer->current();
			$lexer->next();
			
			switch($token->getCode())
			{
				case self::L_BLOB: // alphanum .-
					$data = $token->getData();
					
					$starpos = \mb_strpos($data, '*');
					$level = null;
					if(false !== $starpos)
					{
						$data = (string) \mb_substr($data, 0, $starpos);
						$level = (int) \mb_substr_count($data, '.');
						$level += (int) \mb_substr_count($data, '-');
						$data = \rtrim($data, '.-');
					}
					
					// for ~ operator, reajust the real operator to be used
					if($currentOp->equals(new VersionOperatorNextMinor()))
					{
						$level = 1 + (int) \mb_substr_count($data, '.');
						$level += (int) \mb_substr_count($data, '-');
					}
					
					$version = $this->_versionParser->parse($data);
					
					// for ^ operator, reajust the operator if dev versions
					if($currentOp->equals(new VersionOperatorNextMajor()) && 0 === $version->getMajor())
					{
						$currentOp = new VersionOperatorNextMinor();
					}
					
					if(null !== $level)
					{
						switch($level)
						{
							case 0:
								$currentOp = new VersionOperatorHigherEquals();
								break;
								
							case 1:
								$currentOp = new VersionOperatorNextMajor();
								break;
								
							case 2:
								$currentOp = new VersionOperatorNextMinor();
								break;
								
							default:
								$currentOp = new VersionOperatorNextPatch();
								break;
						}
					}
					
					$constraint = new VersionConstraintSimple($currentOp, $version);
					
					if(null === $lastConstraint)
					{
						$lastConstraint = $constraint;
						break;
					}
					
					if($conjunction)
					{
						$lastConstraint = new VersionConstraintIntersection($lastConstraint, $constraint);
						$currentOp = new VersionOperatorEquals();
						break;
					}
					
					$lastConstraint = new VersionConstraintUnion($lastConstraint, $constraint);
					$currentOp = new VersionOperatorEquals();
					
					break;
					
				case self::L_SPACE: // sp
					// nothing to do
					break;
					
				case self::L_LOWER: // <
					$currentOp = new VersionOperatorStrictlyLower();
					break;
					
				case self::L_UPPER: // >
					$currentOp = new VersionOperatorStrictlyHigher();
					break;
					
				case self::L_EXCL: // !
					$currentOp = new VersionOperatorDifferent();
					break;
					
				case self::L_EQLS: // =
					$currentOp = new VersionOperatorEquals();
					break;
					
				case self::L_TILDE: // ~
					$currentOp = new VersionOperatorNextMinor();
					break;
					
				case self::L_CARET: // ^
					$currentOp = new VersionOperatorNextMajor();
					break;
					
				case self::L_VBAR: // |
					$conjunction = false;
					break;
					
				case self::L_ESPER: // &
					$conjunction = true;
					break;
					
				case self::L_OPPAR: // (
					try
					{
						$constraint = $this->parseLexer($lexer, false);
					}
					catch(ParseThrowable $e)
					{
						break;
					}
					
					if(null === $lastConstraint)
					{
						$lastConstraint = $constraint;
						break;
					}
					
					if($conjunction)
					{
						$lastConstraint = new VersionConstraintIntersection($lastConstraint, $constraint);
						$currentOp = new VersionOperatorEquals();
						break;
					}
					
					$lastConstraint = new VersionConstraintUnion($lastConstraint, $constraint);
					$currentOp = new VersionOperatorEquals();
					
					break;
					
				case self::L_CLPAR: // )
					if(null === $lastConstraint)
					{
						throw new ParseException(VersionConstraintInterface::class, '', 0);
					}
					
					return $lastConstraint;
					
				case self::L_COMMA: // ,
					$conjunction = true;
					break;
					
				case self::OP_LEQ: // <=
					$currentOp = new VersionOperatorLowerEquals();
					break;
					
				case self::OP_UEQ: // >=
					$currentOp = new VersionOperatorHigherEquals();
					break;
					
				case self::OP_OR_: // ||
					$conjunction = false;
					break;
					
				case self::OP_AND: // &&
					$conjunction = true;
					break;
					
				case self::OP_DIF: // != OR <>
					$currentOp = new VersionOperatorDifferent();
					break;
			}
		}
		
		if(null === $lastConstraint)
		{
			throw new ParseException(VersionConstraintInterface::class, '', 0);
		}
		
		return $lastConstraint;
	}
	
}
